import random
from random import shuffle

class Generator:

    @staticmethod
    def generateInitialPopulation(populationSize, data):
        population = []
        #za jedan random izlaz
        for index in range (0,populationSize):
            a = 97
            # dobiti random
            tempdata = [str(x) for x in random.sample(range(0, data.rides_number), data.rides_number)]
            # za svaki vehicle dodati slovo
            for cnt in range (1, data.vehicle_number + 1):
                tempdata.append(chr(a))
                a += cnt

            shuffle(tempdata)
            population.append(tempdata)

        return population

def add(c, x):
  return chr(ord(c)+x)
