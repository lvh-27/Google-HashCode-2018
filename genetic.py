import numpy as np
import random
import parser
import data
import populationGenerator

def distance(start_x, start_y, end_x, end_y):
  return abs(end_x - start_x) + abs(end_y - start_y)

class genalgo(object):
    population = []
    mutationChange = 0.1;
    data = []
    def __init__(self, initPopulation, data = []):
        for pop in initPopulation:
            self.population.append([pop, int(pop[0])])
            #self.population.append([pop, self.evaluate(pop)])
        self.mutationChange = 0.1;
        self.data = data

    def evaluate(self, genome):
        return self.population[0];
        rides = self.data.rides 
        currentRides = []
        totalScore = 0
        for gene in genome:
            if type(gene) is int:
                currentRides.append(rides[gene])
        else:
            timeCounter = 0
            currentX = 0
            currentY = 0
            for currentRide in currentRides:
                distanceToRide = distance(currentX, currentY, currentRide.start_x, currentRide.start_y)
                timeCounter += distanceToRide
                if timeCounter > currentRide.latest_finish:
                    continue
                if timeCounter < currentRide.earliest_start:
                    timeCounter = currentRide.earliest_start
                elif timeCounter == currentRide.earliest_start:
                    totalScore += self.data.bonus

                timeCounter += currentRide.length()
                if timeCounter <= currentRide.latest_finish:
                    totalScore += currentRide.length()

        return totalScore
            
    def selectNewChromosome(self, k):
        best = self.population[0]
        for i in range(k):
            index = random.randint(0, len(self.population)-1)
            ind = self.population[index]
            if ind[1] > best[1]:
                best = self.population[index]
        return best[0]
        pass

    def mutate(self, genome):
        a = random.randint(0, len(genome) - 2)
        b = random.randint(0, len(genome) - 2)
        genome[a], genome[b] = genome[b] , genome[a]
        return genome

    def breed(self, gene_a, gene_b, co_point_perc=0.3):
        def get_shorter_gene(gene_a, gene_b):
            return gene_a if len(gene_a) <= len(gene_b) else gene_b

        # so as not to overflow
        shorter_gene = get_shorter_gene(gene_a, gene_b)
        # number of crossover points, defined as a percentage of
        # the length of the shorter gene
        no_of_co_points = int(co_point_perc * (len(shorter_gene)-1))
        co_points_indices = sorted(np.random.choice(range(len(shorter_gene)), no_of_co_points, replace=False))
        
        i = 0;
        while i in range(no_of_co_points):
            tmp_a = gene_a[:co_points_indices[i]] +\
                    gene_b[co_points_indices[i]:co_points_indices[i+1]] +\
                    gene_a[co_points_indices[i]:]

            tmp_b = gene_b[:co_points_indices[i]] +\
                gene_a[co_points_indices[i]:co_points_indices[i+1]] +\
                gene_b[co_points_indices[i]:]
            i += 2  # two-by-two?

        return ([c for c in tmp_a], [c for c in tmp_b])

    def run(self):
        N = len(self.population)
        newPopulation = []
        iter = 0;
        while iter < 500:
            while len(newPopulation) < N:
                genomeA = self.selectNewChromosome(3)
                genomeB = self.selectNewChromosome(3)
                genomeA, genomeB = self.breed(genomeA, genomeB)
                if random.uniform(0.0, 1.0) < self.mutationChange:
                    genoomeA[0] = mutate(genomeA[0])
                if random.uniform(0.0, 1.0) < self.mutationChange:
                    genoomeB[0] = mutate(genomeB[0])    
                newPopulation.append([genomeA, evaluate(genomeA)])
                newPopulation.append([genomeB, evaluate(genomeB)])
            if iter%20:
                print(max(newPopulation, key=lambda item:item[1]))
                self.population = newPopulation
        pass


data = parse("a_example.in")


population = [["1","2","3","4","5","6","7","8","9"],
              ["2","3","4","5","6","7","8","9","1"], 
              ["3","4","5","6","7","8","9","1","2"], 
              ["9","1","2","3","4","5","6","7","8"]]

genalgo(population).run()
