
class Ride:
  
  def __init__(self, start_x, start_y, end_x, end_y, earliest_start, latest_finish):
    self.start_x = start_x
    self.start_y = start_y
    self.end_x = end_x
    self.end_y = end_y
    self.earliest_start = earliest_start
    self.latest_finish = latest_finish

  def __str__(self):
    return 'From [{0}, {1}] to [{2}, {3}], earliest start: {4}, latest finish: {5}'.format(self.start_x, self.start_y, self.end_x, self.end_y, self.earliest_start, self.latest_finish)

  def length(self):
    return abs(self.end_x - self.start_x) + abs(self.end_y - self.start_y)