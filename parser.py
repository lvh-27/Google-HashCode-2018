import sys
from ride import Ride
from data import Data
from populationGenerator import Generator

def parse(a):
  with open(a) as file:
    lines = file.readlines()
    line1 = lines[0][:-1].split(' ')
    rows = int(line1[0])
    columns = int(line1[1])
    vehicle_number = int(line1[2])
    rides_number = int(line1[3])
    bonus = int(line1[4])
    step_number = int(line1[5])

    rides = []
    for line in lines[1:]:
      parts = line[:-1].split(' ')
      rides.append(Ride(int(parts[0]),int(parts[1]),int(parts[2]),int(parts[3]),int(parts[4]),int(parts[5])))

    return Data(rows, columns, vehicle_number, rides_number, bonus, step_number, rides)


def generateInitialPopulation(data):




    random = Generator.generateInitialPopulation(3, data)

    bar = [[] for item in range(0, data.vehicle_number)]

    a = distance(data.rides[0])



    b = 6

# distanceBetweenNodes

def distance(ride):
    return abs(ride.end_x - ride.start_x) + abs(ride.end_y- ride.start_y)


if __name__ == '__main__':
    data = parse("/Users/ianic/Downloads/a_example.in")

    generateInitialPopulation(data)

    # x = [[empty for i in range(10)] for j in range(10)]

